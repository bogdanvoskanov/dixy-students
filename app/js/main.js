$(document).ready(function () {
  const burgerBtn = document.querySelector('.nav__burger');
  const navMenu = document.querySelector('.nav__menu');

  burgerBtn.addEventListener('click', () => {
    if (!burgerBtn.classList.contains('show')) {
      burgerBtn.classList.add('show');
      navMenu.classList.add('show');
    } else {
      burgerBtn.classList.remove('show');
      navMenu.classList.remove('show');
    }
  });

  if (window.innerWidth <= 500) {
    $('.slider').slick({
      arrows: false,
      infinite: false,
      dots: true
    });
  }
});
